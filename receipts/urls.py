from django.urls import path

from receipts.views import (account_list, category_list, create_account,
                            create_category, create_receipt,
                            show_receipts_models)

urlpatterns = [
    path("", show_receipts_models, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
